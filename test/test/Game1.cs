﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace test
{
    /// <summary>
    /// 基底 Game クラスから派生した、ゲームのメイン クラスです。
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D stage;
        Texture2D chara,enemy,bullet;
        Texture2D hitPointBar;
        Input control;
        Manager manage;        
        SpriteFont font;
        Vector2 windowSize;       
        Vector2 charaOrigin,bulletOrigin;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferHeight = 600;
            graphics.PreferredBackBufferWidth = 800;
        }

        /// <summary>
        /// ゲームが実行を開始する前に必要な初期化を行います。
        /// ここで、必要なサービスを照会して、関連するグラフィック以外のコンテンツを
        /// 読み込むことができます。base.Initialize を呼び出すと、使用するすべての
        /// コンポーネントが列挙されるとともに、初期化されます。
        /// </summary>
        protected override void Initialize()
        {
            // TODO: ここに初期化ロジックを追加します。
            
            base.Initialize();
        }

        /// <summary>
        /// LoadContent はゲームごとに 1 回呼び出され、ここですべてのコンテンツを
        /// 読み込みます。
        /// </summary>
        protected override void LoadContent()
        {
            // 新規の SpriteBatch を作成します。これはテクスチャーの描画に使用できます。
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //windowの大きさを取得

            manage = new Manager();
            windowSize = new Vector2(graphics.GraphicsDevice.Viewport.Width, graphics.GraphicsDevice.Viewport.Height);
            

            control = new Input(this.Window);

            // TODO: this.Content クラスを使用して、ゲームのコンテンツを読み込みます。
            stage = Content.Load<Texture2D>("EDGE1"); 
            chara = Content.Load<Texture2D>("EDGE2"); 
            enemy = Content.Load<Texture2D>("EDGE3"); 
            bullet = Content.Load<Texture2D>("EDGE4");
            hitPointBar = new Texture2D(GraphicsDevice, 1, 1);

            font = Content.Load<SpriteFont>("SpriteFont1");


            //画像の中心点の取得
            charaOrigin = new Vector2(this.chara.Width / 2, this.chara.Height / 2);
            manage.AddUnitSize("player",charaOrigin);
            manage.AddUnitSize("enemy", charaOrigin);
             bulletOrigin = new Vector2(this.bullet.Width / 2, this.bullet.Height / 2);
            manage.AddUnitSize("bullet", bulletOrigin);
            manage.StartStage(windowSize);

        }

        /// <summary>
        /// UnloadContent はゲームごとに 1 回呼び出され、ここですべてのコンテンツを
        /// アンロードします。
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: ここで ContentManager 以外のすべてのコンテンツをアンロードします。
        }

        /// <summary>
        /// ワールドの更新、衝突判定、入力値の取得、オーディオの再生などの
        /// ゲーム ロジックを、実行します。
        /// </summary>
        /// <param name="gameTime">ゲームの瞬間的なタイミング情報</param>
        protected override void Update(GameTime gameTime)
        {
            // ゲームの終了条件をチェックします。
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: ここにゲームのアップデート ロジックを追加します。
            var direction = Direction.None;



            control.update();//キーボードの状態の取得
            if (control.PushCheck(2)) direction |= Direction.Left;
            if (control.PushCheck(3)) direction |= Direction.Right;
            if (control.PushCheck(0)) direction |= Direction.Up;
            if (control.PushCheck(1)) direction |= Direction.Down;

            manage.Update(direction,charaOrigin,bulletOrigin);//キャラの移動などの処理
            
            //Zキー押したら攻撃
            if (control.PushCheck(4))
            {
                if (manage.player.alive)
                {
                    manage.Attack();
                }
                else
                {
                    manage.StartStage(windowSize);
                }
            }
            //Xキー押したら敵が沸く
            if (control.PushCheck(5))
            {
                manage.createEnemy();
            }
            //当たり判定
            manage.hit();
            
            
            base.Update(gameTime);
        }

        /// <summary>
        /// ゲームが自身を描画するためのメソッドです。
        /// </summary>
        /// <param name="gameTime">ゲームの瞬間的なタイミング情報</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: ここに描画コードを追加します。
            spriteBatch.Begin();
            //マップチップの描画
            foreach(var mapTip in manage.map)
            {
                spriteBatch.Draw(stage, mapTip.TexturePositioin, null, Color.White, 0.0f, Vector2.Zero, 2.0f, SpriteEffects.None, 0.0f);
            }

            if (manage.player.alive)
                spriteBatch.Draw(chara,new Vector2(windowSize.X / 2,windowSize.Y / 2),null,Color.White,MathHelper.ToRadians(manage.player.rotate),charaOrigin,2.0f,SpriteEffects.None,0.0f);
            else
                spriteBatch.DrawString(font, "GAME OVER", new Vector2(windowSize.X / 2,windowSize.Y / 2), Color.White);
            //敵の描画
            foreach (var enem in manage.Enemy)
            {
                hitPointBar.SetData<Color>(new Color[]{Color.Red});
                spriteBatch.Draw(hitPointBar,new Rectangle((int)enem.WrightChara.X - 16,(int)enem.WrightChara.Y - 30,enem.HitPoint / 2,10),Color.White);
                spriteBatch.Draw(enemy, enem.WrightChara,null, Color.White,MathHelper.ToRadians(enem.rotate),charaOrigin,2.0f,SpriteEffects.None,0.0f);

            }
            //弾の描画
            foreach (var bul in manage.Bullet)
            {
                spriteBatch.Draw(bullet, bul.WrightChara, null, Color.White, MathHelper.ToRadians(bul.rotate), bulletOrigin, 2.0f, SpriteEffects.None, 0.0f);
            }
            spriteBatch.End();


            base.Draw(gameTime);
        }
    }
}
