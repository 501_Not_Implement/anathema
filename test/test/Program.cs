﻿using System;
using Microsoft.Xna.Framework;

namespace test
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリー ポイントです。
        /// </summary>
        static void Main(string[] args)
        {
            using (Game1 game = new Game1())
            {
                GameWindow wnd = game.Window;
                wnd.Title = "anathema";
                game.Run();
            }
        }
    }
#endif
}

