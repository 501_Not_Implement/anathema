﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace test
{
    class Tile
    {
        public Vector2 TexturePositioin { get { return texturePosition; } }

        private int locationX;
        private int locationY;
        public static int tileWidth = 32, tileHeight = 18;
        private bool floor;
        private Vector2 texturePosition;
       

        public Tile(int x,int y,bool f)
        {
            locationX = x;
            locationY = y;
            floor = f;
        }
        //描画位置の計算
        public void DrawPoint(Vector2 scroll)
        {
            texturePosition.X = tileWidth * (locationX + locationY) - scroll.X;
            texturePosition.Y = tileHeight * (locationX - locationY) + scroll.Y;
        }
    }
}
