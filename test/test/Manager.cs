﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace test
{
    class Manager
    {
        public IEnumerable<Chara> Bullet { get { return bullet; } }
        public IEnumerable<Chara> Enemy { get { return enemy; } }
        

        private List<Chara> bullet;
        private List<Chara> enemy;
        private List<Chara> removeEnemy;
        private List<Chara> removeBullet;
        private Dictionary<String, int[]> unitSize;
        public Tile[,] map;
        private int width = 32, height = 32;
        private Vector2 scroll;
        public Chara player;
        private Random cRandom;
        private int coolTime;
        private int enemTime;
       

        public Manager()
        {
            bullet = new List<Chara>();
            enemy = new List<Chara>();
            removeEnemy = new List<Chara>();
            removeBullet = new List<Chara>();
            map = new Tile[width,height];
            cRandom = new System.Random();
            unitSize = new Dictionary<String, int[]>();
            coolTime = 0;
            enemTime = 0;
        }
        public void StartStage(Vector2 window)
        {
            player = Chara.CreatePlayer(7.0f, 2.0f, 0.0f, ref scroll, window,unitSize["player"]);
            enemy.Clear();
            enemy.Add(Chara.CreateEnemy(4.0f, 3.0f, 0.0f, scroll,unitSize["enemy"],100));
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                {
                    if (i == 0 || i == 31 || j == 0 || j == 31)
                        map[i, j] = new Tile(i, j, false);
                    else
                        map[i, j] = new Tile(i, j, true);
                }
        }
        //新しい敵の生成
        public void createEnemy()
        {
            enemy.Add(Chara.CreateEnemy((float)cRandom.Next(width - 1), (float)cRandom.Next(height - 1), 0.0f,scroll,unitSize["enemy"],100));
            enemTime = 240;
        }
        //ユニットのサイズを格納
        public void AddUnitSize(String name,Vector2 origin)
        {
            int[] size = new int[2]{(int)origin.X * 2, (int)origin.Y * 2};
            unitSize.Add(name, size);
        }
        //Managerのアップデート
        public void Update(Direction direction, Vector2 chOrigin, Vector2 bulOrigin)
        {
            player.Move(direction,ref scroll);
            foreach (var enem in enemy)
            {
                if(HitCheck(player,enem))
                    player.IsAttacked(enem);
            }
            if (coolTime > 0)
            {
                coolTime--;
            }
            if (enemTime > 0)
            {
                enemTime--;
            }
            //弾丸の移動と消える弾丸の区別
            foreach (var bul in bullet)
            {
                if (bul.BulletMove())
                    removeBullet.Add(bul);
                bul.DrawPoint(bulOrigin, scroll);
                bul.GetSize();
            }
            //消える弾丸の消去
            foreach (var del in removeBullet)
            {
                bullet.Remove(del);
            }
            removeBullet.Clear();
            //移動により生じる描画のずれの計算
            foreach (var point in map)
            {
                point.DrawPoint(scroll);
            }
            foreach (var enem in enemy)
            {
                enem.EnemyMove(player);
                enem.DrawPoint(chOrigin,scroll);
                enem.GetSize();
            }
        }
        //弾丸の生成
        public void Attack()
        {
            if (coolTime == 0)
            {
                bullet.Add(new Chara(player.Pos, player.rotate,scroll,20,unitSize["bullet"]));
                coolTime = 30;
            }

        }
        //敵と弾丸の当たり判定
        public void hit()
        {
            
            foreach(var bul in bullet)
                foreach (var ch in enemy)
                {

                    if (HitCheck(bul, ch) && HitCheck(ch,bul))
                    {
                        removeBullet.Add(bul);
                        ch.IsAttacked(bul);
                        if(!ch.alive)
                            removeEnemy.Add(ch);
                    }   
                }
            foreach (var bul in removeBullet)
                bullet.Remove(bul);
            foreach (var enem in removeEnemy)
                enemy.Remove(enem);
            removeBullet.Clear();
            removeEnemy.Clear();
        }
        private bool HitCheck(Chara a, Chara b)
        {
            if (a.Size.Intersects(b.Size))
                {
                    return true;
                }               
            else return false;
        }
        
    }
}
