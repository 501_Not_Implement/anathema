﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using SDXDI = SlimDX.DirectInput;
using SlimDX.DirectSound;


namespace test
{
    class Input
    {
       
        //ボタンの状態に関して
        private bool[] push;
        private int Button = 6;
        //入力機器の構造体
        private KeyboardState keyBoard;
        private GamePadState xPad;
        private bool xCheck;
        private SDXDI.Joystick joyStick;
        private SDXDI.JoystickState directPad;
        private SDXDI.JoystickState directPadStandardValue;
        private readonly int endPlay = 10000;
        

        //public input()
        public Input(GameWindow gw)
        {
            
            push = new bool[Button];
            xCheck = false;
           

            //xinput関連の初期化処理
            xPad = GamePad.GetState(PlayerIndex.One);
            if(xPad.IsConnected)
            {
                xCheck = true;
            }

            //directinput関係の初期化処理
            if (!xCheck)
            {
                directPad = new SDXDI.JoystickState();
                directPadStandardValue = new SDXDI.JoystickState();
                SDXDI.DirectInput dinput = new SDXDI.DirectInput();
                foreach (SDXDI.DeviceInstance device in dinput.GetDevices(SDXDI.DeviceClass.GameController, SDXDI.DeviceEnumerationFlags.AttachedOnly))
                {
                    // deviceの作成
                    try
                    {
                        joyStick = new SDXDI.Joystick(dinput, device.InstanceGuid);
                        joyStick.SetCooperativeLevel(gw.Handle, SDXDI.CooperativeLevel.Foreground);
                        break;
                    }

                    catch (SDXDI.DirectInputException)
                    {
                    }
                }
                // アクセス権の取得を試みる
                if (joyStick != null)
                {
                    this.joyStick.Acquire();
                    directPadStandardValue = this.joyStick.GetCurrentState();
                }
            }
            //入力を初期化する
            PushReset();
        }
        private void PushReset()
        {
            for (int i = 0; i < Button; i++)
            {
                push[i] = false;
            }
        }
        //後々消す予定　number番目の成否を返す
        public bool PushCheck(int number)
        {
            return push[number];
        }
        //Inputのアップデート
        public void update()
        {
            

            PushReset();
            if(xCheck)
            {
                xinputUpdate();
            }
            else if (this.joyStick != null)
            {
                directUpdate();
            }
            keyBoardUpdate();
            
        }
        // directinput系統のJoyStickの入力を取る
        private void directUpdate()
        {
            directPad = this.joyStick.GetCurrentState();

            if (Math.Abs(directPad.X - directPadStandardValue.X) >= endPlay)
            {
                if (directPad.X > directPadStandardValue.X)
                {
                    push[3] = true;
                }
                else
                {
                    push[2] = true;
                }
            }
            
            if (Math.Abs(directPad.Y - directPadStandardValue.Y) > endPlay)
            {
                if (directPad.Y > directPadStandardValue.Y)
                {
                    push[1] = true;
                }
                else
                {
                    push[0] = true;
                }
            }
            
            if (directPad.IsPressed(3))
            {
                push[4] = true;
            }
        }

        //xinput系統のJoyStickの入力を取る
        private void xinputUpdate()
        {
            xPad = GamePad.GetState(PlayerIndex.One);

            if (xPad.ThumbSticks.Left.X < 0)
            {
                push[2] = true;
            }
            if (xPad.ThumbSticks.Left.X > 0)
            {
                push[3] = true;
            }
            if (xPad.ThumbSticks.Left.Y < 0)
            {
                push[1] = true;
            }
            if (xPad.ThumbSticks.Left.Y > 0)
            {
                push[0] = true;
            }
            if (xPad.Buttons.A == ButtonState.Pressed)
            {
                push[4] = true;
            }
        }
        //keyBoardの入力を取る
        private void keyBoardUpdate()
        {
            keyBoard = Keyboard.GetState();
            
            if (keyBoard.IsKeyDown(Keys.Left)) push[2] = true;
            if (keyBoard.IsKeyDown(Keys.Right)) push[3] = true;
            if (keyBoard.IsKeyDown(Keys.Up)) push[0] = true;
            if (keyBoard.IsKeyDown(Keys.Down)) push[1] = true;
            if (keyBoard.IsKeyDown(Keys.Z)) push[4] = true;
            if (keyBoard.IsKeyDown(Keys.X)) push[5] = true;
        }

    }
    //後々使う予定　入力をわかりやすくするため
    [Flags]
    public enum ButtonRole
    {
        Up,Down,Left,Right,Attack
    }
}
