﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;



namespace test
{
    class Chara
    {
        public Vector2 WrightChara { get { return wrightChara; } }
        public Rectangle Size { get { return size; } }
        public int HitPoint { get { return hitPoint; } }

        public Vector2 Pos { get {return pos;} }
        private Vector2 pos;
        private Vector2 wrightChara;        
        private Vector2 currentScroll;
        private Vector2 currentPos;
        private Rectangle size;
        private int attack;
        public static int tileWidth = 32, tileHeight = 18;
        public bool alive { get; set; }
        private int hitPoint;
        private int charaWidth, charaHeight;
        public float rotate { get; set; }

        
        
        //可動物の生成
        public Chara(Vector2 posStart,float rot,Vector2 bg,int hp,int[] unitSize)
        {
            pos.X = posStart.X;
            pos.Y = posStart.Y;
            rotate = rot;
            alive = true;
            charaWidth = unitSize[0];
            charaHeight = unitSize[1];
            wrightChara.X = tileWidth * (pos.X + pos.Y) - bg.X + charaWidth;
            wrightChara.Y = tileHeight * (pos.X - pos.Y) + bg.Y + charaHeight;
            hitPoint = hp;
            attack = 20;
        }
        //プレイヤーキャラ
        public static Chara CreatePlayer(float x, float y, float rot,ref Vector2 bg,Vector2 window,int[] unitSize)
        {
            bg.X = tileWidth * (x + y) + unitSize[0] - (window.X / 2);
            bg.Y = (window.Y / 2) - tileHeight * (x - y) - unitSize[1];
            return new Chara(new Vector2(x, y), rot, bg, 100,unitSize);
        }
        //敵キャラ
        public static Chara CreateEnemy(float x,float y,float rot,Vector2 bg,int[] size,int hp)
        {
            return new Chara(new Vector2(x,y),rot,bg,hp,size);
        }
        //プレイヤーの移動
        public void Move(Direction direction,ref Vector2 scroll)
        {
            currentScroll = scroll;
            currentPos = pos;

            if (pos.X >= 0 && pos.X <= 31 && pos.Y >= 0 && pos.Y <= 31)
            {
                var speed = 0.03f;
                if ((direction & Direction.Left) != 0)
                {
                    pos.X -= speed;
                    pos.Y -= speed;
                    scroll.X += 32 * (-speed - speed);
                    rotate = 180.0f;
                }
                if ((direction & Direction.Right) != 0)
                {
                    pos.X += speed;
                    pos.Y += speed;
                    scroll.X += 32 * (speed + speed);
                    rotate = 0.0f;
                }
                if ((direction & Direction.Up) != 0)
                {
                    pos.Y += speed;
                    pos.X -= speed;
                    scroll.Y -= 18 * (-speed - speed);
                    rotate = 270.0f;
                }
                if ((direction & Direction.Down) != 0)
                {
                    pos.Y -= speed;
                    pos.X += speed;
                    scroll.Y -= 18 * (speed + speed);
                    rotate = 90.0f;
                }

                if ((direction & Direction.Left) != 0 && (direction & Direction.Up) != 0)
                {
                    pos.X -= speed / 2;
                    scroll.X += 32 * (-speed / 2);
                    scroll.Y -= 18 * (-speed / 2);
                    rotate = 225.0f;
                }
                if ((direction & Direction.Right) != 0 && (direction & Direction.Down) != 0)
                {
                    pos.X += speed / 2;
                    scroll.X += 32 * (speed / 2);
                    scroll.Y -= 18 * (speed / 2);
                    rotate = 45.0f;
                }
                if ((direction & Direction.Up) != 0 && (direction & Direction.Right) != 0)
                {
                    pos.Y += speed / 2;
                    scroll.X += 32 * (speed / 2);
                    scroll.Y -= 18 * (-speed / 2);
                    rotate = 315.0f;
                }
                if ((direction & Direction.Down) != 0 && (direction & Direction.Left) != 0)
                {
                    pos.Y -= speed / 2;
                    scroll.X += 32 * (-speed / 2);
                    scroll.Y -= 18 * (speed / 2);
                    rotate = 135.0f;
                }

            }
            if (pos.X < 0 || pos.X > 31 || pos.Y < 0 || pos.Y > 31)
            {
                pos.X = currentPos.X;
                pos.Y = currentPos.Y;
                scroll.X = currentScroll.X;
                scroll.Y = currentScroll.Y;
            }

            GetSize();
           
        }

        public void EnemyMove(Chara target)
        {
            var speed = 0.015f;
            if (pos.X < target.Pos.X)
            {
                pos.X += speed;
            }
            if (pos.X > target.Pos.X)
            {
                pos.X -= speed;
            }
            if (pos.Y < target.Pos.Y)
            {
                pos.Y += speed;
            }
            if (pos.Y > target.Pos.Y)
            {
                pos.Y -= speed;
            }
        }

        //描画位置の獲得
        public void DrawPoint(Vector2 origin,Vector2 scroll)
        {
            wrightChara.X = tileWidth * (pos.X + pos.Y) - scroll.X + size.Width;
            wrightChara.Y = tileHeight * (pos.X - pos.Y) + scroll.Y + size.Height;
            
        }
        //弾の移動
        public bool BulletMove()
        {
            var speed = 0.5f;
            if (rotate == 180.0f)
            {
                pos.X -= speed;
                pos.Y -= speed;
            }
            if (rotate == 0.0f)
            {
                pos.X += speed;
                pos.Y += speed;
            }
            if (rotate == 270.0f)
            {
                pos.Y += speed;
                pos.X -= speed;
            }
            if (rotate == 90.0f)
            {
                pos.Y -= speed;
                pos.X += speed;
            }
            if (rotate == 225.0f)
            {
                pos.X -= speed * 1.2f;
            }
            if (rotate == 45.0f)
            {
                pos.X += speed * 1.2f;
            }
            if (rotate == 315.0f)
            {
                pos.Y += speed * 1.2f;
            }
            if (rotate == 135.0f)
            {
                pos.Y -= speed * 1.2f;
            }
            if (pos.X < 0 || pos.X > 31 || pos.Y < 0 || pos.Y > 31)
            {
                
                return true;
            }
            return false;
        }
        public void GetSize()
        {
            if (size != null)
            {
                size.Width = charaWidth;
                size.Height = charaHeight;
            }
            size.X = (int)wrightChara.X;
            size.Y = (int)wrightChara.Y;
        }
        public void IsAttacked(Chara a)
        {
            hitPoint -= a.attack;
            if (hitPoint <= 0)
            {
                alive = false;
            }
        }
        
        
    }
    [Flags]
    public enum Direction
    {
        None = 0,
        Left = 1,
        Up = 2,
        UpLeft = 3,
        Right = 4,
        UpRight = 6,
        Down = 8,
        DownLeft = 9,
        DownRight = 12
    }
}
